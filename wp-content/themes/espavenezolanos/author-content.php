<?php 
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
//var_dump($curauth->user_email);
//var_dump(get_avatar( get_the_author_meta( 'user_email' ), 170, 'mystery', esc_attr( get_the_author() ) ));
?>

<div class="author-bio <?php echo esc_attr( $type ); ?> post-module et_pb_extra_module module">
	<div class="author-box-avatar">
						<?php echo get_avatar( $curauth->user_email, 170, 'mystery', esc_attr( $curauth->display_name ) ); ?>
				</div>
	<div class="author-page-title"><h1><span class="author-label">Autor: </span><?php echo $curauth->display_name; ?></h1></div>
	<p><?php echo $curauth->description; ?></p>
<?php
								$social_links = '';
								foreach ( extra_get_social_networks() as $network => $title ) {
									$network = esc_attr( $network );
									//var_dump($curauth->$network);
									//echo $author->$network;
									if ( !empty( $curauth->$network ) ) {
										$url = extra_format_url( $curauth->$network );
										$social_links .= sprintf( '<a href="%s" target="_blank"><i class="et-extra-icon et-extra-icon-%s et-extra-icon-author-%s-color-hover"></i></a>',
											esc_url( $url ),
											esc_attr( $network ),
											esc_attr( $curauth->ID )
										);
									}
								}
								if ( !empty( $social_links ) ) {
									echo '<div class="author-footer">' . $social_links . '</div>';
								}
								//var_dump(extra_get_social_networks());
								?>
</div>
<?php $type = strtolower( et_get_option( 'archive_list_style', 'standard' ) ); ?>
<div class="posts-blog-feed-module <?php echo esc_attr( $type ); ?> post-module et_pb_extra_module module">
	<div class="paginated_content">
		<div class="paginated_page" <?php echo 'masonry' == $type ? 'data-columns' : ''; ?>>
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				$post_format = et_get_post_format();
				$post_format_class = !empty( $post_format ) ? 'et-format-' . $post_format : '';
				?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry ' . $post_format_class ); ?>>
						<div class="header">
							<?php
							$thumb_args = array(
								'size'      => 'extra-image-medium',
								'img_after' => '<span class="et_pb_extra_overlay"></span>',
							);
							require locate_template( 'post-top-content.php' );
							?>
						</div>
						<?php
						if ( !in_array( $post_format, array( 'quote', 'link' ) ) ) {
						?>
						<div class="post-content">
							<?php $color = extra_get_post_category_color(); ?>
							<h2 class="post-title entry-title"><a class="et-accent-color" style="color:<?php echo esc_attr( $color ); ?>;" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<div class="post-meta vcard">
								<p><?php echo extra_display_archive_post_meta(); ?></p>
							</div>
							<div class="excerpt entry-summary">
								<p><?php
								if ( has_excerpt() ) {
									the_excerpt();
								} else {
									$excerpt_length = get_post_thumbnail_id() ? '100' : '230';
									et_truncate_post( $excerpt_length );
								}
								?></p>
								<a class="read-more-button" href="<?php the_permalink(); ?>"><?php echo esc_html__( 'Leer más', 'extra' ); ?></a>
							</div>
						</div>
						<?php } ?>
					</article>
				<?php
			endwhile;
		else :
			?>
			<article class='nopost'>
				<h5><?php esc_html_e( 'Sorry, No Posts Found', 'extra' ); ?></h5>
			</article>
			<?php
		endif;
		?>
		</div><!-- .paginated_page -->
	</div><!-- .paginated_content -->

	<?php global $wp_query; ?>
	<?php if ( $wp_query->max_num_pages > 1 ) { ?>
	<div class="archive-pagination">
		<?php echo extra_archive_pagination(); ?>
	</div>
	<?php } ?>
</div><!-- /.posts-blog-feed-module -->
