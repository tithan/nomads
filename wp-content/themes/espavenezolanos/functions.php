<?php
function espavene_enqueue_styles() {
	wp_enqueue_style( 'the-parent-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get('Version'));
  wp_enqueue_script('nomads', get_stylesheet_directory_uri() . '/js/nomads.js', 'jquery', false, true);
  $cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/main.css'));
  wp_enqueue_style('nomads-style', get_stylesheet_directory_uri() . '/main.css', array(), $cache_buster, 'all');

}
add_action( 'wp_enqueue_scripts', 'espavene_enqueue_styles' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
function for_loco_translate() {
echo _e( 'All', 'extra' );
echo _e( 'Popular', 'extra' );
echo _e( 'Latest', 'extra' );
echo _e( 'Top rated', 'extra' );
}
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
  		show_admin_bar(false);
	}
}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        }

    return $title;

});

function nomads_search_form($form) {
  $form = '<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '">
          <label>
            <input type="search" class="search-field" placeholder="Buscar …" value="'. get_search_query() .'" name="s">
          </label>
          <button class="search-submit"><span class="dashicons dashicons-search"></span></button>
        </form>';
        return $form;
}
add_filter( 'get_search_form', 'nomads_search_form' );

function nomads_where_filter($where){
        global $wpdb;
        if( is_search() ) {
            $search= get_query_var('s');
            $query=$wpdb->prepare("SELECT user_id  FROM $wpdb->usermeta WHERE ( meta_key='first_name' AND meta_value LIKE '%%%s%%' ) or ( meta_key='last_name' AND meta_value LIKE '%%%s%%' )", $search ,$search);
            $authorID= $wpdb->get_var( $query );

            if($authorID){
                $where = "  AND  ( wp_posts.post_author = {$authorID} ) ";
            }

         }
         return $where;
    }

    add_filter('posts_where','nomads_where_filter');
?>