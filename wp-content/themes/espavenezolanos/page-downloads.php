<?php
/**
* Template Name: Para zona de descargas de la Web
*
* @package Nómadas con raíces
* @subpackage Espavenezolanos
*/

if (isset($_POST['filetodownload'])) {
	//sleep(10);
	$File = $_POST['filetodownload'];
	/*header("Content-Disposition: attachment; filename=\"" . basename($File) . "\"");
	header("Content-Type: application/force-download");
	header("Content-Length: " . filesize($File));
	header("Connection: close");*/
}
get_header();
?>
<div id="main-content">
	<div class="container">
		<div id="content-area" class="<?php extra_sidebar_class(); ?> clearfix">
			<div class="et_pb_extra_column_main">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="post-wrap">
						<?php if ( is_post_extra_title_meta_enabled() ) { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<?php } ?>
						<div class="post-content entry-content">
							<h2 id="wholetext">La descarga de su archivo <?php echo basename($File) ?> comenzará en <span id="countdown" class="progress"></span> <span class="progress">segundos</span> automáticamente. Si no empieza haga clic en <a href="<?php echo $File ?>" id="downloadfile">descarga manual</a></h2>
							<?php the_content(); ?>
							<?php
							if ( ! extra_is_builder_built() ) {
								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'extra' ),
									'after'  => '</div>',
								) );
							}
							?>
						</div>
					</div><!-- /.post-wrap -->
				</article>
				<?php
					endwhile;
				else :
					?>
					<h2><?php esc_html_e( 'Post not found', 'extra' ); ?></h2>
					<?php
				endif;
				wp_reset_query();
				?>
				<?php
				if ( ( comments_open() || get_comments_number() ) && 'on' == et_get_option( 'extra_show_pagescomments', 'on' ) ) {
					comments_template( '', true );
				}
				?>
			</div><!-- /.et_pb_extra_column.et_pb_extra_column_main -->

			<?php get_sidebar(); ?>

		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer();	?>
<script type="text/javascript">
	var timeleft = 10;
	var downloadTimer = setInterval(function(){
	  document.getElementById("countdown").innerHTML = timeleft;
	  timeleft -= 1;
	  if(timeleft <= 0){
	    clearInterval(downloadTimer);
	    document.getElementById("wholetext").innerHTML = " ";
	    document.getElementById("wholetext").innerHTML = "Descargando...";
	  }
	}, 1000);
	/*setTimeout(function() {
		var href = jQuery('#downloadfile').attr('href');
      	window.location.href = href;
	}, 10000);*/
</script>
