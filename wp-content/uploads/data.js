var botName = "Kyra",
botAvatar = "http://nomadasconraices.com/wp-content/uploads/2018/09/kyra1.png",
conversationData = {"homepage": {1: { "statement": [ 
"¡Hola! Mi nombre es Kyra, soy el robot que mantiene este sitio y me gustaría ser tu asistente aquí.", 
"Tengo solo un par de preguntas.", 
"¿Cómo te llamas?", 
], "input": {"name": "name", "consequence": 1.2}},1.2:{"statement": function(context) {return [ 
"¡Un placer conocerte " + context.name  + "!", 
"Como puedes ver, nuestra web se lanzará muy pronto.", 
"Lo se, estás muy emocionado para verlo, pero necesitamos unos días más para terminarlo.", 
"¿Te gustaría ser el primero en verlo?", 
];},"options": [{ "choice": "Si","consequence": 1.4},{ 
"choice": "No, gracias","consequence": 1.5}]},1.4: { "statement": [ 
"¡Guay! Por favor deja tu correo electrónico aquí y te enviaré un mensaje cuando esté listo. Solo utilizaré tu correo para enviarte información relevante sobre nuestra Web. Puedes leer nuestra políticas de privacidad en el enlace que está abajo en el pié de página.", 
], "email": {"email": "email", "consequence": 1.6}},1.5: {"statement": function(context) {return [ 
"Lamento escuchar eso, " + context.name  + " :( Nos vemos la próxima vez.", 
];}},1.6: { "statement": [ 
"¡Recibido! ¡Gracias y te vemos pronto aquí!", 
"¡Que tengas un buen día!", 
]}}};